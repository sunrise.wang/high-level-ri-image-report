\documentclass[11pt,
english,
singlespacing,
parskip]{article}
\usepackage[bottom = 20ex, left = 20ex, right = 20ex, top = 20ex]{geometry}
\usepackage{hyperref}
\usepackage{graphicx}
\usepackage{amsmath}
\usepackage{amssymb}

\newcommand{\R}{\mathbb{R}}

\title{A Practical Overview of some Radio-Inteferometric Imaging Algorithms}

\author{Sunrise Wang}

\begin{document}
\maketitle

\begin{abstract}
This is the abstract
\end{abstract}

\section{Introduction}
Traditional observation of our universe occurs in the visual frequencies of the electromagnetic (EM) spectrum as this the range we are able to observe with our eyes. More recent technological advances have allowed us to break this limitation, allowing us to also observe natural phenomena at other frequencies ranges.

Different ranges of EM frequencies are given different names, with one such range being radio. This is the lowest range, and refers to all EM waves with a frequency of 300GHz and below. In addition to affording us the ability to observe different natural phenomena, such as unionized hydrogen clouds, observing in radio is also advantageous as much of its range is not blocked by the earth's atmosphere, allowing us clear observations with low-altitude observatories.

Radio observations are traditionally performed using single element instruments, such as a satellite dish. However, these elements are limited in aperture ergo resolution. A more recent method involves the use of interferometry and aperture synthesis, where arrays of antennae are used to simulate large apertures, allowing us to resolve small and distant objects.

An issue with radio-interferometers is that they only sparsely sample the sky, resulting in images that have many visual artefacts. This image, termed the dirty image, can be difficult to study for scientific purposes, thus, imaging algorithms are used to remove the artefacts. 

A pertinent factor to consider when designing and using these algorithms is their processing cost. Due to the large number of samples per observation, these algorithms can take a substantial amount of time and resources. This is particularly problematic for the Square Kilometer Array (SKA) project~\cite{ska}, where the number of samples per observation are even more numerous, and have to be processed within a specified amount of time, as they will be discarded afterwards in order to free up storage for the next observation.

To process the data, a supercomputer, termed the Science Data Processor (SDP), will be used. To better determine its hardware design, it is imperative to determine how various imaging algorithms perform on different hardware layouts. To aid in achieving this, the dataflow prototyping framework PREESM~\cite{pelcat2014preesm} will be used in conjunction with the SimGrid~\cite{casanova2008simgrid} simulation framework to provide performance projections for various algorithms. The idea is for the users to rapidly define various different algorithms using PREESM's dataflow interface, as well as a hardware layout using Simgrid. PREESM will then communicate with Simgrid in order to obtain performance projections for the defined algorithm.

This report is focused on detailing some imaging pipelines that can be tested in this context. Rather than being exhaustive in describing the algorithms mathematically, it aims to isntead provide a general high-level overview, with the focus instead being on how these pipelines can be parallelized. Specifically, it will focus on two different levels of parallelization, one being fine-level parallelization, which can be performed using modern hardware such as GPUs, and the other being coarse-level parallelization which can be performed across different nodes of a distributed computing system.

Finally, the pipelines described in this report will assume that the samples are already captured and corrected. Thus, the initial sampling stage and calibration will be ignored.

\section{The Traditional Major-Minor Cycle Imaging Pipeline}
\begin{figure}[ht]
\centering
\includegraphics[width=\linewidth]{images/top.png}
\caption{Sequential flowchart of the traditional Major-Minor cycle imaging pipeline for radio-interferometry, where deconvolution are the minor cycles, and the gridding to subtraction loop is the major cycle. The red boxes here denote states, and the blue denote processes.}
\label{Fig:Top}
\end{figure}

One of the most common imaging pipelines uses a major-minor cycle approach, based on the Cotton-Schwab CLEAN~\cite{schwab1984relaxing} algorithm. This approach is popular as it has proven to work well in many scenarios, providing both performance and robustness. The pipeline described here is very similar to the SKA Evolutionary Pipeline (SEP)~\cite{sep},  with three main differences:
\begin{itemize}
\item Calibration is ignored.
\item FFT degridding is used instead of DFT degridding.  
\item Imaging for multiple frequencies is described.
\end{itemize}

The pipeline commences by reading in the visibilities and discretizing them to some fine grid. This is required as we are using the FFT to perform degridding, thus, some fine resolution approximation to the original visibilities is required. Afterwards, it performs the major and minor cycle iterations.

In each major cycle, the dirty image for the current iteration (termed the residual image) is created by first interpolating the fine grid visibilities onto a coarse grid in a step called gridding, and then performing an inverse Fast Fourier Transform (iFFT) on this grid. The resampling onto a coarse grid is required as the fine grid typically has many times the resolution of the desired residual image. In addition to discretization, the gridding step also corrects for distortions caused by the w-term with a technique called w-projection. 

During the minor cycles, the sources in the residual are found in an iterative greedy manner. After some number of sources have been found, the minor cycles terminate and the sources are transformed to the Fourier domain and degridded back to their fine grid positions. These are then subtracted from the current visibilities, and the next major cycle begins. This process continues for either some pre-specified number of iterations, or until the residual is predominantly noise.

Finally, to produce the output image, a clean PSF is convolved with all of the sources located during the major-minor cycles. The clean PSF can typically either be a Gaussian, or just the central lobe of the dirty PSF. Figure~\ref{Fig:Top} illustrates a sequential flowchart of this algorithm, with deconvolution referring to the minor cycles, and the gridding to subtraction loop being the major cycle.

There are numerous possibilities for parallelization with this pipeline, both from a fine and coarse grained perspective. This rest of this section will go on to discuss these. A preliminary PREESM description of this pipeline is currently in the works, and can be found at: \url{https://gitlab-research.centralesupelec.fr/sunrise.wang/preesm-majorminorpipeline}.

\subsection{Gridding and Degridding}
\begin{figure}[ht]
\centering
\includegraphics[width=\linewidth]{images/griddingseq.png}
\caption{Sequential flowchart of Gridding which assumes an FFT degridding process is used. The w-projection and gridding kernels are combined together so that only a signal convolution is required with the signal.}
\label{Fig:GSeq}
\end{figure}

\begin{figure}[ht]
\centering
\includegraphics[width=\linewidth]{images/griddingpar.png}
\caption{Gridding can be parallelized on a low level for each coarse grid cell or fine grid visibility, depending on the implementation. These can also be seperated into several regions should we wish to perform the computations on multiple nodes.}
\label{Fig:GPar}
\end{figure}

\begin{figure}[ht]
\centering
\includegraphics[width=\linewidth]{images/degriddingseq.png}
\caption{Sequential flowchart of FFT Degridding. Like gridding, the various convolution kernels are combined together so that only a signal convolution is required with the signal.}
\label{Fig:DGSeq}
\end{figure}

\begin{figure}[ht]
\centering
\includegraphics[width=\linewidth]{images/degriddingpar.png}
\caption{Deridding can be parallelized in a similar manner to gridding, as we can process all fine grid visibilities in tandem. These can also be separated into regions for multi-node processing.}
\label{Fig:DGPar}
\end{figure}


Gridding involves the interpolation of the visibilities, which are approximated on some high resolution fine grid, onto some coarse grid with the same resolution as the residual image through the use of a gridding kernel. This operation is parallelizable at a low level as there is no data dependence between the different grid cells, and is well suited for GPU or other SIMD hardware. For the w-projection part, one can join the w-projection and the gridding kernels through convolution, thus, the visibilities still only need to be convolved with a single kernel.

Depending on the resolution of the grid, it could also be practical to separate the grid into several sub-regions and parallelize those in separate nodes. It should however be noted that since we need to perform an FFT in the next step, these gridded values need to be accumulated in some central node. Figures~\ref{Fig:GSeq} and~\ref{Fig:GPar} show the sequential and parallel flow charts of gridding respectively.

Degridding is effectively the opposite of gridding, where the gridded Fourier domain sources are first super-sampled, and then convolved with a kernel that combines the inverses of the gridding and w-projection kernels. The parallelization can also be performed in a similar manner to gridding. Figures~\ref{Fig:DGSeq} and~\ref{Fig:DGPar} show the sequential and parallel degridding process respectively. 

\subsection{FFT}
There exists several parallel versions of the FFT algorithm. Unfortunately, due to unfamiliarity, the discussion of these algorithms will be postponed to a later date. This section here is primarily just acknowledging that they exist.

\subsection{Deconvolution}
\begin{figure}[ht]
\centering
\includegraphics[width=\linewidth]{images/deconv.png}
\caption{The minor cycle consists of deconvolution, which aims to locate sources in the sky. This is by and large a sequential process, thus we won't consider its parallelism here.}
\label{Fig:Deconv}
\end{figure}

The minor cycle performs what is termed deconvolution. Specifically, this involves finding some number of sources, which are assumed to be actual EM emitters in the sky, rather than artefacts caused by instrumentation response. These sources are found by iteratively finding the brightest points, and then subtracting the PSF from their positions. 

As the information for the next iterations is modified from the subtraction, deconvolution is largely a sequential process. Although there are possibilities for parallelization, such as in cases where we have isolated clusters of sources, allowing us to deconvolve each cluster in isolation, these are often use-case dependent. Thus, for simplicity, we assume for this specific pipeline that this step is completely sequential. Figure~\ref{Fig:Deconv} details the deconvolution loop.

\subsection{Images at Multiple Frequencies}
\begin{figure}[ht]
\centering
\includegraphics[width=\linewidth]{images/frequency.png}
\caption{As there is no interdependence when imaging at different frequencies, we are able to perform these separately in different nodes.}
\label{Fig:MFeq}
\end{figure}
It can be desirable to synthesize a series of images at different electromagnetic frequencies in order to obtain spectral line information. As each frequency band depends on different visibility samples, each image can be processed on independent nodes. Figure~\ref{Fig:MFreq} illustrates how this parallelization would be set up.

It is important here to make a distinction that this is merely performing the imaging algorithm at different frequencies, and is not the same as multi-frequency imaging, which aims to fit Taylor series coefficients to some spectral profile. The latter does in fact have data dependencies across the different EM frequencies, and cannot be simply parallelized across them.

\subsection{Concluding Remarks on Parallelism}
The traditional major-minor cycle imaging pipeline can be parallelized at both a low and high-level at multiple stages. A downside is that there are several stages, eg. deconvolution, that require either sequential execution, or a re-centralisation of the data, resulting in trickier design for multi-node parallelism. An exception to this is when we synthesize images at multiple different frequencies. In this case, each image can be computed in isolation, and thus is well suited for multi-node parallelism. 

\bibliographystyle{abbrv}
\bibliography{references}

\end{document}